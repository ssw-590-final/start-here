# SSW 590 Final Project
Author: Veronica Herzog

[[_TOC_]]
## Lab
Want to get started with the GitOps lab? Check out [`ssw-590-final/infra`](https://gitlab.com/ssw-590-final/infra) to learn more, or check out the [video walkthrough](https://vimeo.com/819746040?share=copy) of the lab on Vimeo.

## Paper

### Introduction
This paper explores the method of GitOps for continuous software delivery. We touch on the concepts of continuous delivery, key principles and benefits of GitOps, and how GitOps can benefit continuous delivery practices. We then introduce a widely-used open source tool for implementing GitOps, Argo CD. Finally, we conclude an overview of a hands-on lab, encouraging readers to gain experience standing up a Kubernetes cluster in Google Cloud Platform, deploying an Argo CD instance, and using the GitOps workflow to deploy a sample application. 

### Continuous Delivery
Before understanding the principles of GitOps, it is important to have a foundational understanding of continuous software delivery. Continuous delivery is the “software development practice where code changes are automatically prepared for a release to production.” Unlike continuous deployment, continuous delivery automates the entire software release cycle, but requires manual approval from a developer before deployment. See Figure 1 for visual representation of the difference.

*Figure 1. Continuous delivery vs. continuous deployment [`[1]`](https://aws.amazon.com/devops/continuous-delivery/)*

![Figure 1. Continuous delivery vs. continuous deployment](./img/cd-amazon.png){width=75%}

Each change committed to source control triggers automated workflows to build, test and deploy to a test environment. This enables developers to iterate faster, with minimal manual intervention. The process by which these revisions are deployed can be achieved in a number of ways. One of these ways is following a GitOps workflow.

### GitOps
GitOps is a framework for enabling continuous delivery of cloud native applications using git. The core principles of the GitOps model is declarative, versioned and immutable, automatically pulled and continuously reconciled delivery [`[2]`](https://www.weave.works/technologies/gitops/). This approach promotes a more developer-centric workflow, as it utilizes tools developers are already familiar with for application development. 

The diagram in Figure 2 provides a high-level visual of a pull-based GitOps workflow. Developers check-in their application code to a git repository. This commit triggers a build of the application container image, which is then pushed to an image repository. The commit also triggers a change to the environment repository, hosting application configuration and infrastructure code. The environment configuration files are updated to reference the newly built image. An operator running in the environment (e.g. a Kubernetes operator [`[3]`](https://kubernetes.io/docs/concepts/extend-kubernetes/operator/)) continuously watches for changes to the image and environment repository. When a change is detected, the operator updates the environment to match what is declared in git. This reconciliation of state works both ways: if a change is made to the environment that is not declared in git, the operator will revert the change to keep the environment consistent with the desired state.

*Figure 2. Pull-based GitOps deployment model [`[4]`](https://www.gitops.tech/)*

![Figure 2. Pull-based GitOps deployment model](./img/pull-gitops.png){width=75%}

Running GitOps workflows as part of the software delivery process has a number of advantages. As mentioned previously, continuous delivery increases developer productivity and experience by enabling faster, automated testing after each committed change. GitOps adds further value by improving the visibility and auditability of changes. Source control becomes an organization’s single source of truth for all application, configuration or infrastructure changes made to an environment. This consistency also adds value to the reliability and maintainability of the system. Development teams have reproducible rollbacks using git reverts, or forks, as well as point in time snapshots of the entire environment to use in disaster recovery scenarios [`[5]`](https://www.weave.works/technologies/gitops/).

The GitOps model provides a set of standards for application development, but does not dictate the specific tools to use. The current most popular industry tools for GitOps delivery are Flux, created by the pioneers of GitOps, and Argo CD. The remainder of this paper explores the use and implementation of Argo CD. 

### Argo CD
Argo CD is an open source GitOps continuous delivery tool, designed for Kubernetes-native environments. Argo CD was developed and released open sourced by Intuit in 2018 [`[6]`](https://www.intuit.com/blog/innovative-thinking/tech-innovation/argo-projects-cncf-graduation/), and has since been implemented by over 250 organizations[`[7]`](https://github.com/argoproj/argo-cd/blob/master/USERS.md). The Argo CD application monitors an organizations’ git repositories and environments to ensure live infrastructure always matches the declaratively-defined resources in source control.

Argo CD uses Kubernetes Custom Resource Definitions (CRDs[`[8]`](https://kubernetes.io/docs/concepts/extend-kubernetes/api-extension/custom-resources)) to define and deploy applications. The CRDs are called `Applications`, which define the source, destination and specific configuration details of a particular application. A high-level diagram of how Argo CD manages GitOps continuous delivery can be found in Figure 3. 

*Figure 3. High-level Argo CD architecture diagram [`[9]`](https://argo-cd.readthedocs.io/en/stable/)*

![Figure 3. High-level Argo CD architecture diagram](./img/argocd.png){width=50%}

Argo CD can be customized to build complex delivery pipelines, and can be further extended by using other Argo project tools such as Argo Workflows to automate both continuous integration and delivery pipelines (CI/CD). 

### Hands-On Lab
The complimentary hands-on lab can be found in the ssw-590-final project on GitLab. Each repository walks through the initializes, configuring and deploying the lab infrastructure. The goal of the lab is to deploy and test the use of Argo CD in a Kubernetes cluster. The lab uses Google Cloud Platform (GCP), as it offers up to $400 in credits for students to use in the first 3-months of opening an account. An overview of the lab repositories and infrastructure is provided in Figure 4. 

*Figure 4. Hands-on Lab high level diagram*

![Figure 4. Hands-on Lab high level diagram](./img/lab-layout.png){width=50%}

The lab uses GitLab CI to automate the deployment of infrastructure, rather than running locally. Links to all of the repositories, referenced below, are listed and linked in below.

The [infra repository](https://gitlab.com/ssw-590-final/infra) manages all foundational cloud infrastructure required to run the lab in Google Cloud. This includes a Virtual Private Network and one subnet, a Google Kubernetes Engine (GKE) cluster provisioned in that subnet, a Billing budget to alert on unexpected charges, and an IAM Service Account to be used by the platform repository. All of this infrastructure is managed using the Infrastructure as Code (IaC) tool, Terraform. 

The [platform repository](https://gitlab.com/ssw-590-final/platform) manages the continuous delivery platform ArgoCD by deploying it to the GKE cluster via Helm. This repository also manages the ApplicationSet that informs ArgoCD to monitor and deploy the cat-app application. 

Finally, the [cat-app repository](https://gitlab.com/ssw-590-final/cat-app) manages the source code and Helm chart for the sample application. When changes are made to this repository, the GitLab CI pipeline runs a build of the container image and publishes it to DockerHub. ArgoCD then monitors for commits to the Helm chart, and updates the deployed Kubernetes application accordingly.

### Conclusion
This paper and associated lab provide insights and hands-on experience with GitOps continuous delivery using Argo CD. GitOps utilizes git repositories to define and deliver applications and infrastructures to cloud-native environments. Implementing GitOps principles enables software development organizations to iterate faster, improve system reliability and consistency, and improve the overall developer experience. We encourage readers to walk through the lab and, if interested, explore other GitOps solutions such as Flux [`[9]`](https://fluxcd.io/flux) to understand and compare what each open-source solution has to offer.
